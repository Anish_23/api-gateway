provider "aws" {
  profile = var.profile
  region  = var.region
}

resource "aws_api_gateway_rest_api" "Alerts" {
  name = "Alerts"
}

resource "aws_api_gateway_resource" "alarm-html" {
  parent_id   = "cpi55mzs07"
  path_part   = "alarm-html"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "alarm-html" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.alarm-html.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "alarm-json" {
  parent_id   = "cpi55mzs07"
  path_part   = "alarm-json"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "alarm-json" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.alarm-json.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "auditlog" {
  parent_id   = "cpi55mzs07"
  path_part   = "auditlog"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "auditlog" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.auditlog.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "cost" {
  parent_id   = "cpi55mzs07"
  path_part   = "cost"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "cost" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.cost.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "instance" {
  parent_id   = "cpi55mzs07"
  path_part   = "instance"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "instance" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.instance.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "sg" {
  parent_id   = "cpi55mzs07"
  path_part   = "sg"
  rest_api_id = "i0p6wxzbkk"
}

resource "aws_api_gateway_method" "sg" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.sg.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_resource" "sg-json" {
  parent_id   = "cpi55mzs07"
  path_part   = "sg-json"
  rest_api_id = "i0p6wxzbkk"
}


resource "aws_api_gateway_method" "sg-json" {
  rest_api_id   = aws_api_gateway_rest_api.Alerts.id
  resource_id   = aws_api_gateway_resource.sg-json.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_stage" "log" {
  deployment_id = "4oebsv"
  rest_api_id   = "i0p6wxzbkk"
  stage_name    = "log"
}
